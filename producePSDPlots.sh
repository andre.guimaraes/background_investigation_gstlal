echo "Producing psd plot for real H1L1 data."

gstlal_plot_psd \
--output plots/H1L1_real_psd.png \
--verbose \
measuredPSDFiles/H1L1_real_measured_psd.xml.gz

echo "Producing psd plot for fake H1L1 data."

gstlal_plot_psd \
--output plots/H1L1_fake_psd.png \
--verbose \
measuredPSDFiles/H1L1_fake_measured_psd.xml.gz

echo "Done!"
