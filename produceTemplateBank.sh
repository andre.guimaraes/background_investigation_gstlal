echo "Creating Template Bank for Real H1 Data"

lalapps_tmpltbank \
--disable-compute-moments \
--grid-spacing Hexagonal \
--dynamic-range-exponent 69.0 \
--enable-high-pass 1024 \
--high-pass-order 8 \
--strain-high-pass-order 8 \
--minimum-mass 1.00 \
--maximum-mass 2.00 \
--min-total-mass 2.00 \
--max-total-mass 4.00 \
--max-eta 0.25 \
--min-eta 0.223 \
--gps-start-time 1264723218 \
--gps-end-time 1264809617 \
--calibrated-data real_8 \
--channel-name H1:DCS-CALIB_STRAIN_C01 \
--space Tau0Tau3 \
--number-of-segments 15 \
--minimal-match 0.9 \
--high-pass-attenuation 0.1 \
--min-high-freq-cutoff ERD \
--segment-length 268435456 \
--low-frequency-cutoff 30.0 \
--pad-data 8 \
--num-freq-cutoffs 1 \
--sample-rate 16384 \
--high-frequency-cutoff 1024 \
--resample-filter ldas \
--strain-high-pass-atten 0.1 \
--strain-high-pass-freq 1024 \
--frame-cache /framesCacheFiles/H1L1_real_frames.cache \
--max-high-freq-cutoff ERD \
--approximant TaylorF2 \
--order twoPN \
--spectrum-type median \
--verbose

#ligolw_no_ilwdchar H1-TMPLTBANK-1264723218.0-2048.xml

#gstlal_inspiral_add_template_ids H1-TMPLTBANK-1264723218.0-2048.xml