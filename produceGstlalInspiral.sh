echo "running gstlal inspiral for real data."

gstlal_inspiral \
--reference-psd measuredPSDFiles/H1L1_real_measured_psd.xml.gz \
--frame-segments-file segmentsFiles/H1L1_real_segments_new.xml.gz \
--frame-cache framesCacheFiles/H1L1_real_frames.cache \
--veto-segments-file /home/gstlalcbc.offline/observing/3/b/C00/chunk24_1257388107_1258128955/vetoes.xml.gz \
--time-slide-file timeSlidesFiles/tisi.xml \
--svd-bank H1:/home/gstlalcbc.offline/observing/3/b/C00/chunk24_1257388107_1258128955/gstlal_svd_bank/H1-0000_SVD-1257388107-740848.xml.gz,\
L1:/home/gstlalcbc.offline/observing/3/b/C00/chunk24_1257388107_1258128955/gstlal_svd_bank/L1-0000_SVD-1257388107-740848.xml.gz \
--output LLOIDFiles/H1L1-0000_LLOID.xml.gz \
--ranking-stat-output DIST_STATSFiles/H1L1-0000_DIST_STATS.xml.gz \
--gps-start-time 1264723218 \
--gps-end-time 1264809617 \
--data-source frames \
--frame-segments-name datasegments \
--channel-name H1=DCS-CALIB_STRAIN_C01 \
--channel-name L1=DCS-CALIB_STRAIN_C01 \
--track-psd \
--psd-fft-length 32 \
--fir-stride 1 \
--singles-threshold inf \
--tmp-space tmp \
--control-peak-time 0 \
--min-instruments 2 \
--ht-gate-threshold 100. \
--coincidence-threshold 0.005 \
--disable-service-discovery \
--verbose