echo "producing gstlal bank mass model file for real data." 
    
gstlal_inspiral_mass_model \
--template-bank /home/gstlalcbc.offline/observing/3/b/C00/chunk24_1257388107_1258128955/gstlal_bank_O3.xml.gz \
--reference-psd MeasuredPSDFiles/H1L1_real_measured_psd.xml.gz \
--output gstlalBankMassModelFiles/H1L1_real-ALL_MASS_MODEL-1264723218-86399.h5 \
--model ligo

echo "producing gstlal bank mass model file for fake data." 

gstlal_inspiral_mass_model \
--template-bank /home/gstlalcbc.offline/observing/3/b/C00/chunk24_1257388107_1258128955/gstlal_bank_O3.xml.gz \
--reference-psd MeasuredPSDFiles/H1L1_fake_measured_psd.xml.gz \
--output gstlalBankMassModelFiles/H1L1_fake-ALL_MASS_MODEL-1264723218-86399.h5 \
--model ligo

echo "Done!"

printf "\a"