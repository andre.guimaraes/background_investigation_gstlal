echo "Producing Real Measured PSD file"

gstlal_reference_psd \
--data-source frames \
--frame-cache framesCacheFiles/H1L1_real_frames.cache \
--gps-start-time 1264723218 \
--gps-end-time 1264809617 \
--channel-name=H1=DCS-CALIB_STRAIN_C01 \
--channel-name=L1=DCS-CALIB_STRAIN_C01 \
--sample-rate 4096 \
--write-psd measuredPSDFiles/H1L1_real_measured_psd.xml.gz \
--verbose 

echo "Producing Fake Measured PSD file"

gstlal_reference_psd \
--data-source frames \
--frame-cache framesCacheFiles/H1L1_fake_frames.cache \
--gps-start-time 1000000000 \
--gps-end-time 1000086399 \
--channel-name=H1=FAKE-STRAIN \
--channel-name=L1=FAKE-STRAIN \
--sample-rate 4096 \
--write-psd measuredPSDFiles/H1L1_fake_measured_psd.xml.gz \
--verbose 