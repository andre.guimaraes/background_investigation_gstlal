echo "Generating fake H1 data."

gstlal_fake_frames \
--data-source=white \
--color-psd ExamplePSDs/H1_psd.xml.gz \
--channel-name=H1=FAKE-STRAIN \
--frame-type=H1_FAKE \
--gps-start-time=1000000000 \
--gps-end-time=1000086399 \
--output-path=fakeFrames \
--block-size=4096 \
--verbose

echo "Generating fake L1 data."

gstlal_fake_frames \
--data-source=white \
--color-psd ExamplePSDs/L1_psd.xml.gz \
--channel-name=L1=FAKE-STRAIN \
--frame-type=L1_FAKE \
--gps-start-time=1000000000 \
--gps-end-time=1000086399 \
--output-path=fakeFrames \
--block-size=4096 \
--verbose

echo "Done!"