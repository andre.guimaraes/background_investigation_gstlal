echo "Producing horizon plots for real H1L1 data."

gstlal_plot_psd_horizon \
plots/H1L1_real_psd_horizon.png \
measuredPSDFiles/H1L1_real_measured_psd.xml.gz

echo "Producing horizon plots for fake H1L1 data."

gstlal_plot_psd_horizon \
plots/H1L1_fake_psd_horizon.png \
measuredPSDFiles/H1L1_fake_measured_psd.xml.gz
