
echo "Producing dummy veto file for H1."

echo 999999997 999999998 | ligolw_segments \
--insert-from-segwizard=H1=/dev/stdin \
--name=vetoes \
--output vetoesFiles/H1L1_vetoes.xml.gz

echo "Producing dummy veto file for L1."

echo 999999997 999999998 | ligolw_segments \
--insert-from-segwizard=L1=/dev/stdin \
--name=vetoes \
vetoesFiles/H1L1_vetoes.xml.gz 

echo "Doing that thing to .xml.gz files."

ligolw_no_ilwdchar vetoesFiles/H1L1_vetoes.xml.gz 