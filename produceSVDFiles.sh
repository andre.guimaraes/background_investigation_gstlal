# H1-0000_SVD-$(START)-$(DURATION).xml.gz: H1-0000_GSTLAL_SPLIT_BANK-0-0.xml.gz measured_psd.xml.gz
# 	# gstlal_svd_bank_0001
# 	$(TIME) gstlal_svd_bank \
# 		--reference-psd measured_psd.xml.gz \
# 		--samples-min $(SAMPLES_MIN) \
# 		--bank-id 0_0 \
# 		--write-svd $@ \
# 		--ortho-gate-fap 0.5 \
# 		--template-bank $< \
# 		--flow $(LOW_FREQUENCY_CUTOFF) \
# 		--svd-tolerance 0.9999 \
# 		--samples-max-64 2048 \
# 		--clipleft 25 \
# 		--autocorrelation-length 351 \
# 		--samples-max-256 $(SAMPLES_MAX_256) \
# 		--clipright 25 \
# 		--samples-max $(SAMPLE_RATE)
# 	@echo ""

echo "Creating SVD 0000 for H1"

gstlal_svd_bank \
--reference-psd measuredPSDFiles/H1L1_real_measured_psd.xml.gz \
--samples-min 2048 \
--bank-id 0_0 \
--write-svd SVDFiles/H1-0000_SVD-1264723218-86399.xml.gz \
--ortho-gate-fap 0.5 \
--template-bank /home/gstlalcbc.offline/observing/3/b/C00/chunk24_1257388107_1258128955/H1_split_bank_0/H1-0000_GSTLAL_SPLIT_BANK-0-0.xml.gz \
--flow 30.0 \
--svd-tolerance 0.9999 \
--samples-max-64 2048 \
--clipleft 25 \
--autocorrelation-length 351 \
--samples-max-256 2048 \
--clipright 25 \
--samples-max 4096
