echo "Generating real H1 segments file."

echo "1264723218 1264809617" | ligolw_segments \
--insert-from-segwizard=H1=/dev/stdin \
--name=datasegments \
--output segmentsFiles/H1L1_real_segments.xml.gz

echo "Generating real L1 segments file."

echo "1264723218 1264809617" | ligolw_segments \
--insert-from-segwizard=L1=/dev/stdin \
--name=datasegments \
segmentsFiles/H1L1_real_segments.xml.gz

echo "Doing the thing to xml.gz files for real data."

ligolw_no_ilwdchar segmentsFiles/H1L1_real_segments.xml.gz

echo "Generating fake H1 segments file."

echo "1000001727 1000084671" | ligolw_segments \
--insert-from-segwizard=H1=/dev/stdin \
--name=datasegments \
--output segmentsFiles/H1L1_fake_segments.xml.gz

echo "Generating fake L1 segments file."

echo "1000001727 1000084671" | ligolw_segments \
--insert-from-segwizard=L1=/dev/stdin \
--name=datasegments \
segmentsFiles/H1L1_fake_segments.xml.gz

echo "Doing the thing to xml.gz files for fake data."

ligolw_no_ilwdchar segmentsFiles/H1L1_real_segments.xml.gz